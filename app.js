const http = require("http"); 

const express = require("express");


const bodyparser = require("body-parser");

const app = express();

app.set("view engine","ejs");

app.use(express.static( __dirname +"/public"));
app.use(bodyparser.urlencoded({extended:true}));


let datos = [{
    matricula:"2019030399   ",
    nombre:"ACOSTA ORTEGA JESUS HUMBERTO    ",
    sexo:'M ',
    materias:["Ingles", "Base de datos", "Tecnologia I  "]
},
{
    matricula:" 2020030310   ",
    nombre:" ACOSTA VARELA IRVING GUADALUPE",
    sexo:' M ',
    materias:[" Ingles", " Base de datos", " Tecnologia I  "]
},
{
    matricula:" 202003007    ",
    nombre:" ALMOGABAR VAZQUES YARLEN DE JESUS   ",
    sexo:' F ',
    materias:["Ingles", " Base de datos", " Tecnologia I  "]
}

]
app.get("/",(req,res)=>{

    res.render('index',{titulo: "Pagina en Embedded JavaScript EJS",listado:datos ,nombre: "yamir ezequiel paez ayala", grupo: "8-3"});
    
});



app.get("/tabla", (req, res)=> {
    const params = {
        numero: req.query.numero
    }
    res.render("tabla", params);
});

app.post("/tabla", (req, res)=> {
    const params = {
        numero: req.body.numero
    }
    res.render("tabla", params);
});

//cotizacion

app.get("/cotizacion", (req, res)=> {
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazo
    
    }
    res.render("cotizacion", params);
});

app.post("/cotizacion", (req, res)=> {
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazo
    }
    res.render("cotizacion", params);
});

/* La página del error va al final de get/post */
app.use((req, res, next)=> {
    res.status(404).sendFile(__dirname + '/public/error.html');
});

const puerto = 3000;

app.listen(puerto, ()=> {
    console.log("Iniciando puerto");
});